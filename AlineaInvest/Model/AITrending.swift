//
//  AITrending.swift
//  AlineaInvest
//
//  Created by Anand on 11/21/20.
//  Copyright © 2020 Anand. All rights reserved.
//

import UIKit

struct AITrending {

    var trendingItems:[TrendingItem]?
    var trendingName:String?
    
}
struct TrendingItem {
    var title : String?
    var id: String?
    var imgUrl : String?
    var subTitle: String?
    var shareValue: Float?
}
