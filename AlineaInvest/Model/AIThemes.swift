//
//  AIThemes.swift
//  AlineaInvest
//
//  Created by Anand on 11/21/20.
//  Copyright © 2020 Anand. All rights reserved.
//

import UIKit

struct AITheme {
    var themeName : String?
    var themeId: String?
    var cImgURL : String?
}
