//
//  AICategory.swift
//  AlineaInvest
//
//  Created by Anand on 11/20/20.
//  Copyright © 2020 Anand. All rights reserved.
//

import UIKit

struct AICategory {
    var cName : String?
    var cId: String?
    var cImgURL : String?
    var colorCode: String?
}
