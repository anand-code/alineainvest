//
//  AIViewStateProtocol.swift
//  AlineaInvest
//
//  Created by Anand on 11/20/20.
//  Copyright © 2020 Anand. All rights reserved.
//

import UIKit



enum ViewState {
    case initial
    case loading
    case hideLoading
    case categoriesRecieved
    case trendingRecieved
    case themesRecieved
    case dataNotReceived(error: String)
    
}
 protocol AIViewStateProtocol: class {
    func showInitial()
    func showLoading()
    func hideLoading()
    func categoriesRecieved()
    func trendingRecieved()
    func themesRecieved()
    func dataNotReceived(error: String)
}
extension AIViewStateProtocol{
    func showInitial(){
        print("Initial")
    }
    func showLoading(){
        print("show loading")

    }
    func hideLoading(){
        print("hide loading")

    }
    func dataNotReceived(error: String){
        print(error)

    }
    func categoriesRecieved(){
        print("categories recieved")
    }
    func trendingRecieved(){
        print("trending recieved")

    }
    func themesRecieved(){
        print("themes recieved")

    }

    
}

