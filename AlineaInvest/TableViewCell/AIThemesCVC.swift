//
//  AIThemesCVC.swift
//  AlineaInvest
//
//  Created by Anand on 11/21/20.
//  Copyright © 2020 Anand. All rights reserved.
//

import UIKit

class AIThemesCVC: UICollectionViewCell {
        
    
    var theme:AITheme? {
        didSet {
            guard let item = theme else {return}
            if let name = item.themeName {
                nameLabel.text = name
            }
            
            if let themeId = item.themeId {
                themeImage.image = UIImage(named: themeId.lowercased())
            }
            
        }
    }
    
    let themeImage: UIImageView = {
        let imgView = UIImageView()
        imgView.contentMode = .scaleAspectFit
        imgView.translatesAutoresizingMaskIntoConstraints = false
        return imgView
    }()
    
    
    let nameLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.boldSystemFont(ofSize: 18)
        label.textColor = UIColor.black
        label.text = "Name of the label"
        label.textAlignment = .center
        label.numberOfLines = 0

        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.backgroundColor = UIColor.init(hexString: "#FAFBFC")
        self.layer.cornerRadius = 20
        self.clipsToBounds = true
        addViews()
    }
        
    
    func addViews(){
        
        addSubview(themeImage)
        addSubview(nameLabel)
      
        
        themeImage.leftAnchor.constraint(equalTo: leftAnchor, constant: 20).isActive = true
        themeImage.topAnchor.constraint(equalTo: topAnchor, constant: 20).isActive = true
        themeImage.heightAnchor.constraint(equalToConstant: frame.size.width - 80).isActive = true
        themeImage.widthAnchor.constraint(equalToConstant: frame.size.width - 80).isActive = true
        
        nameLabel.leftAnchor.constraint(equalTo: leftAnchor, constant: 20).isActive = true
        nameLabel.rightAnchor.constraint(equalTo: rightAnchor, constant: -20).isActive = true
        nameLabel.topAnchor.constraint(equalTo: themeImage.bottomAnchor, constant: 10).isActive = true
        nameLabel.centerXAnchor.constraint(equalTo: themeImage.centerXAnchor, constant: 0).isActive = true
        
       
        
    }
    
    
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
