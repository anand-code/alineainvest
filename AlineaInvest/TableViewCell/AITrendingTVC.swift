//
//  AITrendingTVC.swift
//  AlineaInvest
//
//  Created by Anand on 11/21/20.
//  Copyright © 2020 Anand. All rights reserved.
//

import UIKit

class AITrendingTVC: UITableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    var trendingItem:TrendingItem? {
        didSet {
            guard let item = trendingItem else {return}
            if let name = item.title {
                titleLabel.text = name
            }
            
            if let subTitle = item.subTitle {
                subTitleLabel.text = subTitle.uppercased()
                categoryImageView.image = UIImage(named: subTitle.lowercased())
            }
            
            if let stock = item.shareValue {
                stockValueLabel.text = "\(stock)%"
                if stock > 0 {
                    stockValueLabel.backgroundColor = UIColor.init(hexString: "#3FDBB4")
                } else if stock < 0 {
                    stockValueLabel.backgroundColor = UIColor.init(hexString: "#FD6A6B")
                }else {
                    stockValueLabel.backgroundColor = UIColor.init(hexString: "#8895BB")
                    
                }
            }
        }
    }
    
    let containerView:UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.clipsToBounds = true // this will make sure its children do not go out of the boundary
        return view
    }()
    let categoryImageView:UIImageView = {
        let img = UIImageView()
//        img.contentMode = .scaleAspectFit // image will never be strecthed vertially or horizontally
        img.translatesAutoresizingMaskIntoConstraints = false // enable autolayout
        img.layer.cornerRadius = 25
        img.clipsToBounds = true
        return img
    }()
    let titleLabel:UILabel = {
        let label = UILabel()
        label.font = UIFont.boldSystemFont(ofSize: 18)
        label.textColor = .black
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let subTitleLabel:UILabel = {
        let label = UILabel()
        label.font = UIFont.boldSystemFont(ofSize: 14)
        label.textColor = .gray
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let stockValueLabel:UILabel = {
        let label = UILabel()
        label.font = UIFont.boldSystemFont(ofSize: 14)
        label.textColor = .white
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textAlignment = .center
        label.layer.cornerRadius = 10
        label.clipsToBounds = true

        return label
    }()
    
    let bottomLine:UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.clipsToBounds = true // this will make sure its children do not go out of the boundary
        view.backgroundColor = UIColor.lightGray
        return view
    }()
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        self.selectionStyle = .none

        
        self.contentView.addSubview(containerView)
        containerView.addSubview(categoryImageView)
        containerView.addSubview(titleLabel)
        containerView.addSubview(subTitleLabel)
        containerView.addSubview(stockValueLabel)
        containerView.addSubview(bottomLine)

        
        containerView.topAnchor.constraint(equalTo:self.contentView.topAnchor,constant: 0).isActive = true
        containerView.leftAnchor.constraint(equalTo:self.contentView.leftAnchor,constant: 0).isActive = true
        containerView.rightAnchor.constraint(equalTo:self.contentView.rightAnchor,constant: 0).isActive = true
        containerView.bottomAnchor.constraint(equalTo:self.contentView.bottomAnchor,constant: 0).isActive = true

        categoryImageView.leftAnchor.constraint(equalTo:containerView.leftAnchor,constant: 10).isActive = true
        categoryImageView.centerYAnchor.constraint(equalTo: containerView.centerYAnchor, constant: 0).isActive = true
        categoryImageView.widthAnchor.constraint(equalToConstant: 50).isActive = true
        categoryImageView.heightAnchor.constraint(equalToConstant: 50).isActive = true
        
        titleLabel.leftAnchor.constraint(equalTo:categoryImageView.rightAnchor,constant: 10).isActive = true
        titleLabel.topAnchor.constraint(equalTo:containerView.topAnchor,constant: 20).isActive = true
        titleLabel.rightAnchor.constraint(equalTo:stockValueLabel.rightAnchor,constant: 10).isActive = true

        subTitleLabel.leftAnchor.constraint(equalTo:categoryImageView.rightAnchor,constant: 10).isActive = true
        subTitleLabel.topAnchor.constraint(equalTo:titleLabel.bottomAnchor,constant: 5).isActive = true
        subTitleLabel.rightAnchor.constraint(equalTo:stockValueLabel.rightAnchor,constant: 10).isActive = true

        
        stockValueLabel.rightAnchor.constraint(equalTo:containerView.rightAnchor,constant: -20).isActive = true
        stockValueLabel.centerYAnchor.constraint(equalTo: containerView.centerYAnchor, constant: 0).isActive = true
        stockValueLabel.widthAnchor.constraint(equalToConstant: 80).isActive = true
        stockValueLabel.heightAnchor.constraint(equalToConstant: 20).isActive = true

        bottomLine.leftAnchor.constraint(equalTo:self.contentView.leftAnchor,constant: 0).isActive = true
        bottomLine.rightAnchor.constraint(equalTo:self.contentView.rightAnchor,constant: 0).isActive = true
        bottomLine.bottomAnchor.constraint(equalTo:self.contentView.bottomAnchor,constant: 0).isActive = true
        bottomLine.heightAnchor.constraint(equalToConstant: 1.0).isActive = true

        
    }

}
