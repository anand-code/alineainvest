//
//  AICategoryTVC.swift
//  AlineaInvest
//
//  Created by Anand on 11/20/20.
//  Copyright © 2020 Anand. All rights reserved.
//

import UIKit

class AICategoryTVC: UITableViewCell {

    var category:AICategory? {
        didSet {
            guard let categoryItem = category else {return}
            if let name = categoryItem.cName {
                categoryImageView.image = UIImage(named: name)
                nameLabel.text = name
            }
            if let colorCode = categoryItem.colorCode {
                self.containerView.backgroundColor = UIColor(hexString: colorCode)
            }

          
        }
    }
    
    let containerView:UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.clipsToBounds = true // this will make sure its children do not go out of the boundary
        view.layer.cornerRadius = 40
        return view
    }()
    

    
    let categoryImageView:UIImageView = {
        let img = UIImageView()
        img.contentMode = .scaleAspectFit // image will never be strecthed vertially or horizontally
        img.translatesAutoresizingMaskIntoConstraints = false // enable autolayout
        img.layer.cornerRadius = 25
        img.clipsToBounds = true
        return img
    }()
    
    let nameLabel:UILabel = {
        let label = UILabel()
        label.font = UIFont.boldSystemFont(ofSize: 20)
        label.textColor = .white
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    

    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.selectionStyle = .none
        
        self.contentView.addSubview(containerView)
        containerView.addSubview(categoryImageView)
        containerView.addSubview(nameLabel)

        containerView.topAnchor.constraint(equalTo:self.contentView.topAnchor,constant: 10).isActive = true
        containerView.leftAnchor.constraint(equalTo:self.contentView.leftAnchor,constant: 10).isActive = true
        containerView.rightAnchor.constraint(equalTo:self.contentView.rightAnchor,constant: -10).isActive = true
        containerView.bottomAnchor.constraint(equalTo:self.contentView.bottomAnchor,constant: -10).isActive = true
        
        categoryImageView.leftAnchor.constraint(equalTo:containerView.leftAnchor,constant: 10).isActive = true
        categoryImageView.centerYAnchor.constraint(equalTo: containerView.centerYAnchor, constant: 0).isActive = true
        categoryImageView.widthAnchor.constraint(equalToConstant: 50).isActive = true
        categoryImageView.heightAnchor.constraint(equalToConstant: 50).isActive = true
        
        nameLabel.leftAnchor.constraint(equalTo:categoryImageView.rightAnchor,constant: 10).isActive = true
        nameLabel.centerYAnchor.constraint(equalToSystemSpacingBelow: containerView.centerYAnchor, multiplier: 0).isActive = true
        nameLabel.rightAnchor.constraint(equalTo:containerView.rightAnchor,constant: -10).isActive = true

    }
    
    required init?(coder aDecoder: NSCoder) {
        
        super.init(coder: aDecoder)
    }
}
