//
//  AIViewModel.swift
//  AlineaInvest
//
//  Created by Anand on 11/20/20.
//  Copyright © 2020 Anand. All rights reserved.
//

import UIKit


class AIViewModel: NSObject {
    
    weak var delegate: AIViewStateProtocol?
    var arrCategories:[AICategory] = []
    var arrTrendingItems:[AITrending] = []
    var arrThemes:[AITheme] = []

    var state:ViewState = .initial{
        didSet {
            switch state {
                case .initial:
                    delegate?.showInitial()
                case .loading:
                    delegate?.showLoading()
                case .hideLoading:
                    delegate?.hideLoading()
                case .categoriesRecieved:
                    delegate?.categoriesRecieved()
                case .trendingRecieved:
                    delegate?.trendingRecieved()
                case .themesRecieved:
                    delegate?.themesRecieved()

                case .dataNotReceived(error: let error):
                    delegate?.dataNotReceived(error: error)
            }
        }
    }
    
    init(delegate: AIViewStateProtocol) {
        self.delegate = delegate
    }
    
    func viewLoaded() {
        state = .initial
    }
    
    func getCategories(){
        setCategories()
        self.state = .categoriesRecieved
    }
    func getTrendingItems(){
        setTrendingItems()
        self.state = .trendingRecieved
    }
    func getThemes(){
        setThemes()
        self.state = .themesRecieved
    }
    func setCategories() {
        
        arrCategories = [
            AICategory(cName: "Stocks", cId: "1", cImgURL: "", colorCode: "#A5A4FD"),
            AICategory(cName: "ETFs", cId: "2", cImgURL: "", colorCode: "#4E4FCF"),
            AICategory(cName: "Crypto", cId: "3", cImgURL: "", colorCode: "#FED73E"),
        ]
    }
    func setTrendingItems() {
        
        arrTrendingItems = [
            AITrending(trendingItems: [TrendingItem(title: "Medifast", id: "1", imgUrl: "", subTitle: "Medi", shareValue: 50.78),TrendingItem(title: "Pinterest", id: "2", imgUrl: "", subTitle: "Pins", shareValue: -4.77),TrendingItem(title: "Slack Technologies", id: "3", imgUrl: "", subTitle: "Work", shareValue: -5.99),TrendingItem(title: "Evoqua Water", id: "3", imgUrl: "", subTitle: "Aqua", shareValue: 5.99)], trendingName: "Top Gainers"),
            AITrending(trendingItems: [TrendingItem(title: "Slack Technologies", id: "1", imgUrl: "", subTitle: "Work", shareValue: -5.99),TrendingItem(title: "Evoqua Water", id: "2", imgUrl: "", subTitle: "Aqua", shareValue: 5.99)], trendingName: "Top Sellers")
        ]
    }
    func setThemes() {
        
        arrThemes = [
            AITheme(themeName: "Diversity & Inclusion", themeId: "1"),
            AITheme(themeName: "Bold Biotech", themeId: "2"),
            AITheme(themeName: "Crypto Central", themeId: "3"),
            AITheme(themeName: "She runs it", themeId: "4"),
            AITheme(themeName: "Clean & Green" , themeId: "5"),
            AITheme(themeName: "Cannabis-ness", themeId: "6"),
            AITheme(themeName: "Power It", themeId: "7"),
            AITheme(themeName: "Foodie Fun", themeId: "8"),
            AITheme(themeName: "Art & Fashion", themeId: "9"),
            AITheme(themeName: "Home is where is the heart", themeId: "10"),

        ]
    }
    
}
