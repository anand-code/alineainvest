//
//  AICategoryVC.swift
//  AlineaInvest
//
//  Created by Anand on 11/19/20.
//  Copyright © 2020 Anand. All rights reserved.
//

import UIKit

class AICategoryVC: UIViewController {

    lazy var vModel = AIViewModel(delegate: self)
    let cellIdentifier = "AICategoryTVC"
    var tblViewCategories : UITableView = {
        let tblView = UITableView()
        tblView.translatesAutoresizingMaskIntoConstraints = false
        return tblView

    }()
    override func viewDidLoad() {
        super.viewDidLoad()

        let window: UIWindow = UIApplication.shared.windows.first!
        let rootViewController = window.rootViewController as! UITabBarController
        let bottomPadding = window.safeAreaInsets.bottom
        let height = rootViewController.tabBar.frame.height + bottomPadding

        vModel.getCategories()
        self.view.addSubview(tblViewCategories)
        tblViewCategories.topAnchor.constraint(equalTo:view.topAnchor).isActive = true
        tblViewCategories.leftAnchor.constraint(equalTo:view.leftAnchor).isActive = true
        tblViewCategories.rightAnchor.constraint(equalTo:view.rightAnchor).isActive = true
        tblViewCategories.bottomAnchor.constraint(equalTo:view.bottomAnchor, constant: -(height)).isActive = true
        
        tblViewCategories.delegate = self
        tblViewCategories.dataSource = self
//        tblViewCategories.register(UINib.init(nibName: cellIdentifier, bundle: .main), forCellReuseIdentifier: cellIdentifier)
        tblViewCategories.register(AICategoryTVC.self, forCellReuseIdentifier: cellIdentifier)

        tblViewCategories.separatorStyle = .none

    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
extension AICategoryVC : UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return vModel.arrCategories.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as? AICategoryTVC
        cell?.category = vModel.arrCategories[indexPath.row]

        return cell ?? UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
}
extension AICategoryVC : AIViewStateProtocol{
    
    func categoriesRecieved() {
        print(self.vModel.arrCategories)
    }
    
}
