//
//  AITrendingVC.swift
//  AlineaInvest
//
//  Created by Anand on 11/19/20.
//  Copyright © 2020 Anand. All rights reserved.
//

import UIKit

class AITrendingVC: UIViewController {

    lazy var vModel = AIViewModel(delegate: self)
    let cellIdentifier = "AITrendingTVC"
    var tblViewTrending : UITableView = {
        let tblView = UITableView()
        tblView.translatesAutoresizingMaskIntoConstraints = false
        return tblView
        
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        
  
        vModel.getTrendingItems()
        self.view.addSubview(tblViewTrending)
        
        let window: UIWindow = UIApplication.shared.windows.first!
        let rootViewController = window.rootViewController as! UITabBarController
        let bottomPadding = window.safeAreaInsets.bottom
        let height = rootViewController.tabBar.frame.height + bottomPadding

        
        tblViewTrending.topAnchor.constraint(equalTo:view.topAnchor).isActive = true
        tblViewTrending.leftAnchor.constraint(equalTo:view.leftAnchor).isActive = true
        tblViewTrending.rightAnchor.constraint(equalTo:view.rightAnchor).isActive = true
        tblViewTrending.bottomAnchor.constraint(equalTo:view.bottomAnchor,constant: -(height)).isActive = true
        
        tblViewTrending.delegate = self
        tblViewTrending.dataSource = self
        tblViewTrending.register(AITrendingTVC.self, forCellReuseIdentifier: cellIdentifier)        
        tblViewTrending.separatorStyle = .none

    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension AITrendingVC : UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return vModel.arrTrendingItems[section].trendingItems?.count ?? 0
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return vModel.arrTrendingItems.count

    }
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return vModel.arrTrendingItems[section].trendingName
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as? AITrendingTVC
        
        cell?.trendingItem = vModel.arrTrendingItems[indexPath.section].trendingItems?[indexPath.row]
        
        return cell ?? UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
    func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int){
        view.tintColor = UIColor.white
        let header = view as! UITableViewHeaderFooterView
        header.textLabel?.textColor = UIColor.black
    }
}
extension AITrendingVC : AIViewStateProtocol{
    
    func trendingRecieved() {
        tblViewTrending.reloadData()
        
    }
    
}
