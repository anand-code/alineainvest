//
//  AIThemesVC.swift
//  AlineaInvest
//
//  Created by Anand on 11/19/20.
//  Copyright © 2020 Anand. All rights reserved.
//

import UIKit

class AIThemesVC: UIViewController {
//    var collectionview: UICollectionView!
    
    let cellIdentifier = "AIThemesCVC"
    lazy var vModel = AIViewModel(delegate: self)
   
    let collectionview : UICollectionView = {
        // init the layout
        let layout = UICollectionViewFlowLayout()
        // set the direction to be horizontal
        layout.scrollDirection = .vertical
        
        // the instance of collectionView
        
        let cv = UICollectionView(frame: .zero, collectionViewLayout: layout)
        
        // Activate constaints
        
        cv.translatesAutoresizingMaskIntoConstraints = false
        cv.backgroundColor = .white
        
        return cv
        
    }()

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.


        self.view.addSubview(collectionview)
        collectionview.register(AIThemesCVC.self, forCellWithReuseIdentifier: cellIdentifier)

        collectionview.dataSource = self
        collectionview.delegate = self
        // setup constrainst
        // make it fit all the space of the CustomCell
        
        let window: UIWindow = UIApplication.shared.windows.first!
        let rootViewController = window.rootViewController as! UITabBarController
        let bottomPadding = window.safeAreaInsets.bottom
        let height = rootViewController.tabBar.frame.height + bottomPadding


        collectionview.topAnchor.constraint(equalTo: self.view.topAnchor).isActive = true
        collectionview.leftAnchor.constraint(equalTo: self.view.leftAnchor).isActive = true
        collectionview.bottomAnchor.constraint(equalTo:self.view.bottomAnchor,constant: -(height)).isActive = true
        collectionview.rightAnchor.constraint(equalTo: self.view.rightAnchor).isActive = true
        
        
    
        vModel.getThemes()

    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */


}
extension AIThemesVC : UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return vModel.arrThemes.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionview.dequeueReusableCell(withReuseIdentifier: cellIdentifier, for: indexPath) as? AIThemesCVC
        cell?.theme = vModel.arrThemes[indexPath.row]
        return cell ?? UICollectionViewCell()

    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let flowayout = collectionViewLayout as? UICollectionViewFlowLayout
        let space: CGFloat = (flowayout?.minimumInteritemSpacing ?? 0.0) + (flowayout?.sectionInset.left ?? 0.0) + (flowayout?.sectionInset.right ?? 0.0)
        let size:CGFloat = (collectionview.frame.size.width - space) / 2.0
        return CGSize(width: size, height: size)
    }
    
}
extension AIThemesVC : AIViewStateProtocol{
    
    func themesRecieved() {
        collectionview.reloadData()
    }
    
}
