//
//  AISearchVC.swift
//  AlineaInvest
//
//  Created by Anand on 11/19/20.
//  Copyright © 2020 Anand. All rights reserved.
//

import UIKit

class AISearchVC: UISimpleSlidingTabController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupUI()
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    private func setupUI(){
        // view
        view.backgroundColor = .white
        
        // navigation
        navigationItem.title = "Explore"
        navigationController?.navigationBar.barTintColor = .white
        navigationController?.navigationBar.isTranslucent = false
        navigationController?.navigationBar.shadowImage = UIImage()
        navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.black]
        
        // slidingTab
        addItem(item: AICategoryVC(), title: "Category") // add first item
        addItem(item: AIThemesVC(), title: "Themes") // add second item
        addItem(item: AITrendingVC(), title: "Trending") // add third item
        setHeaderActiveColor(color: .blue) // default blue
        setHeaderInActiveColor(color: .black) // default gray
        setHeaderBackgroundColor(color: .clear) // default white
        setStyle(style: .fixed) // default fixed
        build() // build
        
        
        let leftBarButton = UIBarButtonItem(image: UIImage(named: "hamburger"), style: .plain, target: self, action: nil)
        self.navigationItem.leftBarButtonItem  = leftBarButton
        let rightBarButton = UIBarButtonItem(image: UIImage(named: "notification"), style: .plain, target: self, action: nil)
        self.navigationItem.rightBarButtonItem  = rightBarButton
        

    }
   
}

