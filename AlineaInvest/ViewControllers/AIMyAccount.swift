//
//  AIMyAccount.swift
//  AlineaInvest
//
//  Created by Anand on 11/19/20.
//  Copyright © 2020 Anand. All rights reserved.
//

import UIKit

class AIMyAccount: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setUpUI()
    }
    
    func setUpUI(){
        navigationItem.title = "My Account"
        navigationController?.navigationBar.barTintColor = .white
        navigationController?.navigationBar.isTranslucent = false
        navigationController?.navigationBar.shadowImage = UIImage()
        navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.black]
        let leftBarButton = UIBarButtonItem(image: UIImage(named: "hamburger"), style: .plain, target: self, action: nil)
        self.navigationItem.leftBarButtonItem  = leftBarButton
        
        let rightBarButton = UIBarButtonItem(image: UIImage(named: "notification"), style: .plain, target: self, action: nil)
        self.navigationItem.rightBarButtonItem  = rightBarButton
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
