//
//  AITabBarController.swift
//  AlineaInvest
//
//  Created by Anand on 11/22/20.
//  Copyright © 2020 Anand. All rights reserved.
//

import UIKit

class AITabBarController: UITabBarController {
    override func viewDidLoad() {
        super.viewDidLoad()
        // update first item
        
        DispatchQueue.main.async {
            self.setSelection(item: self.tabBar.selectedItem!)

        }
      
    }
    
    // MARK: - UITabBarDelegate handlers
    
    override func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {
        clearSelections()
        setSelection(item: item)
    }
    
    // MARK: - Selection Methods
    
    func setSelection(item:UITabBarItem) {
        DispatchQueue.main.async {
            for i in 0..<self.tabBar.items!.count {
                let y = self.tabBar.items![i]
                if(item == y) {
                    let sv = self.tabBar.subviews[i+1]
                    sv.backgroundColor = UIColor.init(hexString: "#4E4FCF") // Selection color
                }
            }
        }
    }
    
    func clearSelections() {
        for s in tabBar.subviews {
            s.backgroundColor = UIColor.clear
        }
    }
}
