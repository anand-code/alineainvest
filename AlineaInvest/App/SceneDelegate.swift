//
//  SceneDelegate.swift
//  AlineaInvest
//
//  Created by Anand on 11/19/20.
//  Copyright © 2020 Anand. All rights reserved.
//

import UIKit

class SceneDelegate: UIResponder, UIWindowSceneDelegate {

    var window: UIWindow?

    var homeTabNavigationController : UINavigationController!
    var searchTabNavigationControoller : UINavigationController!
    var trendsTabNavigationController : UINavigationController!
    var myAccountTabNavigationControoller : UINavigationController!
    var aboutUsTabNavigationController : UINavigationController!

    func scene(_ scene: UIScene, willConnectTo session: UISceneSession, options connectionOptions: UIScene.ConnectionOptions) {
        // Use this method to optionally configure and attach the UIWindow `window` to the provided UIWindowScene `scene`.
        // If using a storyboard, the `window` property will automatically be initialized and attached to the scene.
        // This delegate does not imply the connecting scene or session are new (see `application:configurationForConnectingSceneSession` instead).
        
        guard let scene = (scene as? UIWindowScene) else { return }
        window = UIWindow(frame: UIScreen.main.bounds)
        window?.windowScene = scene
        window?.backgroundColor = UIColor.white
        
        
        let tabBarController = AITabBarController()
        
        homeTabNavigationController = UINavigationController.init(rootViewController: AIHomeVC())
        searchTabNavigationControoller = UINavigationController.init(rootViewController: AISearchVC())
        trendsTabNavigationController = UINavigationController.init(rootViewController: AITrendsVC())
        myAccountTabNavigationControoller = UINavigationController.init(rootViewController: AIMyAccount())
        aboutUsTabNavigationController = UINavigationController.init(rootViewController: AIAboutUsVC())
        
        tabBarController.viewControllers = [homeTabNavigationController, searchTabNavigationControoller, trendsTabNavigationController, myAccountTabNavigationControoller, aboutUsTabNavigationController]
        
        
        let item1 = UITabBarItem(title: "", image: UIImage(named: "home"), tag: 0)
        let item2 = UITabBarItem(title: "", image:  UIImage(named: "search"), tag: 1)
        let item3 = UITabBarItem(title: "", image:  UIImage(named: "aboutus"), tag: 2)
        let item4 = UITabBarItem(title: "", image:  UIImage(named: "myaccount"), tag: 3)
        let item5 = UITabBarItem(title: "", image:  UIImage(named: "aboutus"), tag: 4)
        
        homeTabNavigationController.tabBarItem = item1
        searchTabNavigationControoller.tabBarItem = item2
        trendsTabNavigationController.tabBarItem = item3
        myAccountTabNavigationControoller.tabBarItem = item4
        aboutUsTabNavigationController.tabBarItem = item5
        
        tabBarController.tabBar.backgroundColor = .white

        UITabBar.appearance().tintColor = UIColor.white
        
        self.window?.rootViewController = tabBarController
        tabBarController.tabBar.clipsToBounds = true
        tabBarController.tabBar.barTintColor = .white

        tabBarController.tabBar.layer.shadowColor = UIColor.lightGray.cgColor
        tabBarController.tabBar.layer.shadowOpacity = 0.5
        tabBarController.tabBar.layer.shadowOffset = CGSize.zero
        tabBarController.tabBar.layer.shadowRadius = 10
        tabBarController.tabBar.layer.borderColor = UIColor.clear.cgColor
        tabBarController.tabBar.layer.borderWidth = 0
        tabBarController.tabBar.layer.cornerRadius = 20
        tabBarController.tabBar.clipsToBounds = false
        tabBarController.tabBar.backgroundColor = UIColor.white
        UITabBar.appearance().shadowImage = UIImage()
        UITabBar.appearance().backgroundImage = UIImage()

        window?.makeKeyAndVisible()
        
    }

    func sceneDidDisconnect(_ scene: UIScene) {
        // Called as the scene is being released by the system.
        // This occurs shortly after the scene enters the background, or when its session is discarded.
        // Release any resources associated with this scene that can be re-created the next time the scene connects.
        // The scene may re-connect later, as its session was not neccessarily discarded (see `application:didDiscardSceneSessions` instead).
    }

    func sceneDidBecomeActive(_ scene: UIScene) {
        // Called when the scene has moved from an inactive state to an active state.
        // Use this method to restart any tasks that were paused (or not yet started) when the scene was inactive.
    }

    func sceneWillResignActive(_ scene: UIScene) {
        // Called when the scene will move from an active state to an inactive state.
        // This may occur due to temporary interruptions (ex. an incoming phone call).
    }

    func sceneWillEnterForeground(_ scene: UIScene) {
        // Called as the scene transitions from the background to the foreground.
        // Use this method to undo the changes made on entering the background.
    }

    func sceneDidEnterBackground(_ scene: UIScene) {
        // Called as the scene transitions from the foreground to the background.
        // Use this method to save data, release shared resources, and store enough scene-specific state information
        // to restore the scene back to its current state.
    }


}

